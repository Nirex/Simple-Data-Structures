//� 2018 NIREX ALL RIGHTS RESERVED

#ifndef _N_QUEUE_H_
#define _N_QUEUE_H_

template <typename T>
class Queue
{
private:
	struct Node
	{
		T element;
		Node* next;
	};

	Node* first;
	Node* last;

public:
	Queue() : first(NULL), last(NULL)
	{ }

	bool IsEmpty() const
	{
		return first == NULL;
	}

	void Enqueue(const T x)
	{
		if (IsEmpty())
		{
			first = new Node(x);
			last = first;
		}

		else
		{
			Node* p = new Node(x);

			last->next = p;
			last = last->next;
		}
	}

	T Dequeue()
	{
		T x;
		Node* p;
		if (!IsEmpty())
		{
			x = first->element;

			p = first;
			first = first->next;

			delete p;

			return x;
		}
	}

	void MakeEmpty()
	{
		while (!IsEmpty())
			Dequeue();
	}
};

#endif // !_N_QUEUE_H_
