//� 2018 NIREX ALL RIGHTS RESERVED

#ifndef _N_BINARY_TREE_H_
#define _N_BINARY_TREE_H_

#include <iostream>

template <typename T>
class BinaryTree
{
private:
	struct Node
	{
		T data;
		Node* left;
		Node* right;
	};

	Node* root;

	Node* MakeEmpty(Node* tree)
	{
		if (tree == NULL)
		{
			return NULL;
		}
		else
		{
			MakeEmpty(tree->left);
			MakeEmpty(tree->right);
			delete tree;
		}
		return NULL;
	}

	Node* Insert(T x, Node* tree)
	{
		if (tree == NULL)
		{
			tree = new Node;
			tree->data = x;
			tree->left = tree->right = NULL;
		}
		else if (x < tree->data)
		{
			tree->left = Insert(x, tree->left);
		}
		else if (x > tree->data)
		{
			tree->right = Insert(x, tree->right);
		}
		return tree;
	}

	Node* FindMin(Node* tree)
	{
		if (tree == NULL)
		{
			return NULL;
		}
		else if (tree->left == NULL)
		{
			return tree;
		}
		else
		{
			return FindMin(tree->left);
		}
	}

	Node* FindMax(Node* tree)
	{
		if (tree == NULL)
		{
			return NULL;
		}
		else if (tree->right == NULL)
		{
			return tree;
		}
		else
		{
			return FindMax(t->right);
		}
	}

	Node* Remove(T x, Node* tree)
	{
		Node* temp;
		if (tree == NULL)
		{
			return NULL;
		}
		else if (x < tree->data)
		{
			tree->left = Remove(x, tree->left);
		}
		else if (x > tree->data)
		{
			tree->right = Remove(x, tree->right);
		}
		else if (tree->left && tree->right)
		{
			temp = FindMin(tree->right);
			tree->data = temp->data;
			tree->right = Remove(tree->data, tree->right);
		}
		else
		{
			temp = tree;
			if (tree->left == NULL)
			{
				tree = tree->right;
			}
			else if (tree->right == NULL)
			{
				tree = tree->left;
			}
			delete temp;
		}
		return tree;
	}

	Node* Find(Node* tree, int x)
	{
		if (tree == NULL)
		{
			return NULL;
		}
		else if (x < tree->data)
		{
			return find(tree->left, x);
		}
		else if (x > tree->data)
		{
			return find(tree->right, x);
		}
		else
		{
			return tree;
		}
	}

	void PreOrder(Node* tree)
	{
		if (tree == NULL)
		{
			return;
		}
		else
		{
			PreOrder(tree->left);
			std::cout << tree->data << " ";
			PreOrder(tree->right);
		}
	}

	void PostOrder(Node* tree)
	{
		if (tree == NULL)
		{
			return;
		}
		else
		{
			InOrder(tree->right);
			std::cout << tree->data << " ";
			InOrder(tree->left);
		}
	}

public:
	BinaryTree()
	{
		root = NULL;
	}

	~BinaryTree()
	{
		root = MakeEmpty(root);
	}

	void Add(T x)
	{
		root = Insert(x, root);
	}

	void Delete(T x)
	{
		root = Remove(x, root);
	}

	void PreDisplay(void)
	{
		PreOrder(root);
		std::cout << std::endl;
	}

	void PostDisplay(void)
	{
		PostOrder(root);
		std::cout << std::endl;
	}

	void Search(T x)
	{
		root = Find(root, x);
	}
};

#endif // !_N_BINARY_TREE_H_