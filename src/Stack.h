//� 2018 NIREX ALL RIGHTS RESERVED

#ifndef _N_STACK_H_
#define _N_STACK_H_

#include <iostream>

template<typename T>
class Stack
{
private:
	int m_size;
	int top;
	T* arr;

public:
	Stack(int size)
		: m_size(size)
	{
		arr = new T[size];
	}

	Stack(const Stack& rhs)
	{
		delete arr;
		arr = new arr[rhs.m_size];
		m_size = rhs.m_size;
		for (size_t i = 0; i < m_size; i++)
		{
			arr[i] = rhs.arr[i];
		}
	}

	~Stack()
	{
		delete arr;
	}

	Stack& operator=(const Stack &)
	{
		delete arr;
		arr = new arr[rhs.m_size];
		m_size = rhs.m_size;
		for (size_t i = 0; i < m_size; i++)
		{
			arr[i] = rhs.arr[i];
		}
		return *this;
	}

	bool Push(T x)
	{
		if (top == m_size - 1)
		{
			return false;
		}
		else
		{
			a[++top] = x;
			return true;
		}
	}

	T Pop()
	{
		if (isEmpty())
		{
			return false;
		}
		else
		{
			return a[(top--)];
		}
	}

	bool IsFull()
	{
		return (top == m_size - 1)
	}

	bool IsEmpty()
	{
		if (top == -1)
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	T Peek()
	{
		if (isEmpty())
		{
			return NULL;
		}
		else
		{
			return a[top];
		}
	}

	void Print()
	{
		std::cout << "Stack: ";

		for (size_type i = 0; i <= top; ++i)
		{
			std::cout << a[i] << " ";
		}

		std::cout << std::endl;
	}
};

#endif // !_N_STACK_H_
