//� 2018 NIREX ALL RIGHTS RESERVED

#ifndef _N_SINGLY_LINKED_LIST_H_
#define _N_SINGLY_LINKED_LIST_H_

#include <iostream>

template <typename T>
class LinkedList
{
private:
	struct Node
	{
		T data;
		Node* next;
	};

	Node* head;
	Node* tail;
public:
	LinkedList()
	{
		head = NULL;
		tail = NULL;
	}

	void CreateNode(T value)
	{
		Node* temp = new Node;
		temp->data = value;
		temp->next = NULL;

		if (head == NULL)
		{
			head = temp;
			tail = temp;
			temp = NULL;
		}
		else
		{
			tail->next = temp;
			tail = temp;
		}
	}

	void Display()
	{
		Node* temp = new Node;
		temp = head;
		while (temp != NULL)
		{
			std::cout << temp->data << "->";
			temp = temp->next;
		}
		std::cout << "NULL" << std::endl;
	}

	void Insert_Start(T value)
	{
		Node* temp = new Node;
		temp->data = value;
		temp->next = head;
		head = temp;
	}

	void Insert_End(T value)
	{
		Node* temp = new Node;
		temp->data = value;
		temp->next = NULL;

		if (head == NULL)
		{
			head = temp;
			tail = temp;
			temp = NULL;
		}
		else
		{
			tail->next = temp;
			tail = temp;
		}
	}

	void Insert_Position(int pos, T value)
	{
		Node* pre = new Node;
		Node* cur = new Node;
		Node* temp = new Node;
		cur = head;

		for (int i = 1; i < pos; i++)
		{
			pre = cur;
			cur = cur->next;
		}

		temp->data = value;
		pre->next = temp;
		temp->next = cur;
	}

	void Delete_First()
	{
		Node* temp = new Node;
		temp = head;
		head = head->next;
		delete temp;
	}

	void Delete_Last()
	{
		Node* current = new Node;
		Node* previous = new Node;
		current = head;

		while (current->next != NULL)
		{
			previous = current;
			current = current->next;
		}

		tail = previous;
		previous->next = NULL;
		delete current;
	}

	void Delete_Position(int pos)
	{
		Node* current = new Node;
		Node* previous = new Node;
		current = head;

		for (int i = 1; i < pos; i++)
		{
			previous = current;
			current = current->next;
		}

		previous->next = current->next;
	}

	void Sort()
	{
		Node* ptr;
		Node* s;

		T value;
		if (head == NULL)
		{
			return;
		}

		ptr = head;
		while (ptr != NULL)
		{
			for (s = ptr->next; s != NULL; s = s->next)
			{
				if (ptr->data > s->data)
				{
					value = ptr->data;
					ptr->data = s->data;
					s->data = value;
				}
			}
			ptr = ptr->next;
		}
	}

	void Reverse()
	{
		Node* ptr1;
		Node* ptr2;
		Node* ptr3;

		if (head == NULL)
		{
			return;
		}

		if (head->next == NULL)
		{
			return;
		}

		ptr1 = head;
		ptr2 = ptr1->next;
		ptr3 = ptr2->next;
		ptr1->next = NULL;
		ptr2->next = ptr1;

		while (ptr3 != NULL)
		{
			ptr1 = ptr2;
			ptr2 = ptr3;
			ptr3 = ptr3->next;
			ptr2->next = ptr1;
		}

		head = ptr2;
	}

	Node* Search(T value)
	{
		bool flag = false;

		if (head == NULL)
		{
			return nullptr;
		}

		Node* s;
		s = head;
		while (s != NULL)
		{
			if (s->data == value)
			{
				flag = true;
				return s;
			}
			s = s->next;
		}
		if (!flag)
		{
			return nullptr;
		}
	}

	Node* SearchBefore(T value)
	{
		bool flag = false;

		if (head == NULL)
		{
			return nullptr;
		}

		Node* s;
		Node* x;
		s = head;
		x = nullptr;
		while (s != NULL)
		{
			if (s->data == value)
			{
				flag = true;
				return x;
			}
			x = s;
			s = s->next;

		}
		if (!flag)
		{
			return nullptr;
		}
	}
};

#endif // !_N_SINGLY_LINKED_LIST_H_
