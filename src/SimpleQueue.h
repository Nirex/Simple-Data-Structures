//� 2018 NIREX ALL RIGHTS RESERVED

#ifndef _N_SIMPLE_QUEUE_H_
#define _N_SIMPLE_QUEUE_H_

template <typename T>
class QueueSimple
{
private:
	int count;
	int size;

	int head;
	int tail;
	T* dataArray;

public:
	QueueSimple(int size)
		: head(0)
		, tail(0)
		, count(0)
		, size(size)
	{
		dataArray = new T[size];
	}

	bool IsEmpty() const
	{
		return count == 0;
	}

	bool IsFull() const
	{
		return count == size;
	}

	bool Push(const T x)
	{
		if (IsFull())
		{
			return false;
		}

		dataArray[tail] = item;

		tail = (tail + 1) % size;
		count++;

		return true;
	}

	bool Pop(T& out)
	{
		if (IsEmpty())
		{
			return false;
		}

		out = dataArray[head];
		
		head = (head + 1) % size;
		count--;

		return true;
	}

	void MakeEmpty()
	{
		while (!IsEmpty())
			Dequeue();
	}
};

#endif // !_N_SIMPLE_QUEUE_H_
